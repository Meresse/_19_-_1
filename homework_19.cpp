
#include <iostream>

using namespace std;

class Animal
{
public:
    virtual void voice() const = 0;

    virtual ~Animal() 
    {
        cout << "Animal dies !" << endl;
    };
};

class Dog : public Animal
{
public:
    void voice() const
    {
        cout << "woof !" << endl;
    }

    ~Dog() 
    {
        cout << "Dog dies !" << endl;
    };
};

class Cat : public Animal
{
public:
    void voice() const
    {
        cout << "meow !" << endl;
    }

    ~Cat() 
    {
        cout << "Cat dies !" << endl;
    }
};

class Hamster : public Animal
{
public:
    void voice() const
    {
        cout << "squeak !" << endl;
    }

    ~Hamster()
    {
        cout << "Hamster dies !" << endl;
    }
};

int main()
{
    Animal* array[3] = { nullptr };

    array[0] = new Dog;
    array[1] = new Cat;
    array[2] = new Hamster;

    cout << endl << "voices of animals" << endl << endl;

    for (int i = 0; i < 3; i++) {

        array[i]->voice();
 
    }

    cout << endl << "order of destruction" << endl << endl;

    for (int i = 0; i < 3; i++) {

        delete array[i];
        array[i] = nullptr;

    }

    return 0;
}

